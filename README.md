## Miko-chan's Dotnet EditorConfig

This repository contains my preferred text editor settings for Visual Studio projects.  
This is the standard ```.editorconfig``` for all of my .NET Projects.

You can also access and bookmark this project's page to always download the latest version:  
[Project Page](https://mikochan-caat.gitlab.io/dotnet-editorconfig)
