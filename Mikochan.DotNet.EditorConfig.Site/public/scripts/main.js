(() => {
    'use strict';

    var downloadLink = document.querySelector('a[download]');
    var bypass = false;
    if (downloadLink) {
        downloadLink.addEventListener('click', async (evt) => {
            if (bypass) {
                bypass = false;
                return;
            }

            evt.preventDefault();
            try {
                var data = await (await fetch(downloadLink.href)).text();
                var downloadName = downloadLink.getAttribute('download');
                var file = new File([data], `.${downloadName}`, { type: "application/octet-stream;charset=utf-8" });
                saveAs(file);
            } catch (err) {
                console.error(err);
                console.warn("Can't perform custom download, falling back to standard.");
                bypass = true;
                downloadLink.click();
            }
        });
    }
})();
